# 7 Features of a Profitable Rental Property

### 1. Neighborhood
The neighborhood in which you buy will determine the types of tenants you attract and your vacancy rate. If you buy near a university, chances are that students will dominate your pool of potential tenants and you could struggle to fill vacancies every summer. Be aware that some towns try to discourage rental conversions by imposing exorbitant permit fees and piling on red tape.

### 2. Property Taxes
Property taxes likely will vary widely across your target area, and you want to be aware of how much you'll be losing. High property taxes are not always a bad thing—in a great neighborhood that attracts long-term tenants, for example, but there are unappealing locations that also have high taxes.

The municipality's assessment office will have all the tax information on file, or you can talk to homeowners in the community. Be sure to find out if property tax increases are probable in the near future. A town in financial distress may hike taxes far beyond what a landlord can realistically charge in rent.

### 3. Schools 
Consider the quality of the local schools if you're dealing with family-sized homes. Although you will be mostly concerned about monthly cash flow, the overall value of your rental property comes into play when you eventually sell it. If there are no good schools nearby, it can affect the value of your investment.

**Checkout:-**  [Chileno bay real estate for sale](https://ownincabo.com/san-jose-corridor-cabo/chileno-bay-cabo/)

### 4. Crime
No one wants to live next door to a hot spot of criminal activity. The local police or public library should have accurate crime statistics for neighborhoods. Check the rates for vandalism, and for serious and petty crimes, and don't forget to note if criminal activity is on the rise or declining. You might also want to ask about the frequency of a police presence in your neighborhood.

### 5. Job Market 
Locations with growing employment opportunities attract more tenants. To find out how a specific area rates for job availability, check with the U.S. Bureau of Labor Statistics (BLS) or visit a local library. If you see an announcement about a major company moving to the area, you can be sure that workers in search of a place to live will flock there. This may cause housing prices to go up or down, depending on the type of business involved. You can assume that if you would like that company in your backyard, your renters will as well.

### 6. Amenities
Tour the neighborhood and check out the parks, restaurants, gyms, movie theaters, public transportation links, and all the other perks that attract renters. City Hall may have promotional literature that can give you an idea of where the best blend of public amenities and private property can be found.

### 7. Future Development
The municipal planning department will have information on developments or plans that have already been zoned into the area. If there is a lot of construction going on, it is probably a good growth area. Watch out for new developments that could hurt the price of surrounding properties. Additional new housing could also compete with your property. 
